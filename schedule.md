---
layout: page
title: 
---

### Working group meeting 1: Week of 25 October 2021  
The main focus of this group is on the Objective 1 meta-analysis. Our goal is to develop a working statistical model, figures/sketches, and an outline of the resulting manuscript. We will also set aside time to discuss Objective 2 and other potential side projects.  

#### Monday, 25 October

- ***No small group meeting for Europe/Asia timezones***

- **Plenary meeting** at [12.30 UTC](https://www.timeanddate.com/worldclock/converter.html?iso=20211007T123000&p1=24&p2=438&p3=41&p4=1377&p5=241) [0730 CST, 1430 Europe, 1800 India, 2030 Taiwan]  
  - *Introduction to sDiv*, presentation by SH (0.5 hr?)  
  - *Presentation and Discussion* on historical overview of plant-soil feedbacks: theory and empirical approaches (1 hr, led by GK). [*Link to slides*](https://docs.google.com/presentation/d/1XU0TBqbfq_Ni3SFi7D0YhOt93hMQlCkd4IxpaYduAto/edit?usp=sharing)  
  - *Presentation* on the state of the field, especially with respect to meta-analyses of plant-soil feedback (0.5-1 hr, led by GK and XY). [*Link to slides part 1*](https://docs.google.com/presentation/d/1WRu45F--zM21q7xblyGvQ7stV9kVvF5IiFJA8y_20tc/edit?usp=sharing); [*Link to slides part 2*](https://docs.google.com/presentation/d/11hTIf_Aja1XlHhFBxsnCAFbiWwQkutGv/edit?usp=sharing&ouid=113480391510291108115&rtpof=true&sd=true)  
  *Includes an overview of the data available to us as we start*. 
  - *Discussion* planning sub-group activities (0.5 hr)

- **Small group meeting** for the N/S American time zones at 1030 AM CST
  - *Xinyi, Adriana, Gaurav*
  - Discussion: What should the meta-analysis model look like (in a broad way)? Note that PSF data is pairwise, trait data is at the species level. 
  - Implement a working model in Metafor
  - Generate a "to-do" list for the Europe/Asia subgroup

- **Overall deliverable** for Day 1 is a running model with functional traits as predictors of ND and FD 

------------------------------

#### Tuesday, 26 October
- **Small group meeting** for the Europe/Asia time zones at [8:30 UTC](https://www.timeanddate.com/worldclock/converter.html?iso=20211007T083000&p1=24&p2=438&p3=41&p4=1377&p5=241) [1030 Europe, 1400 India, 1630 Taiwan]  
  - *Meghna, Yadu, Po-Ju, Gyuri*
  - Continue discussion about meta-analysis model structure
  - Convert model to BRMS

- **Plenary session** at [12.30 UTC](https://www.timeanddate.com/worldclock/converter.html?iso=20211007T123000&p1=24&p2=438&p3=41&p4=1377&p5=241) [0730 CST, 1430 Europe, 1800 India, 2030 Taiwan]
  - Present and discuss preliminary results from both sub-groups
  - Figures discuss ways to depict results, key elements to have in graphics
  - Discussion: Biological basis to link different traits to stabilization/fitness differences

- **Small group meeting** for the N/S American time zones at 1030 AM CST 
  - Modify model/traits following Plenary discussion
  - Make figures following on Plenary discussion

- **Overall deliverables** for Day 2 are a working model with functional traits as predictors of ND and FD [in BRMS], and figures (potentially still in sketch form)

----------------------------------------------

#### Wednesday, 27 October

- **Small group meeting** for the Europe/Asia time zones at [8:30 UTC](https://www.timeanddate.com/worldclock/converter.html?iso=20211007T083000&p1=24&p2=438&p3=41&p4=1377&p5=241) [1030 Europe, 1400 India, 1630 Taiwan]  
  - Continue working on analysis
  - Continue working on figures
  - Maybe write up the results, if we feel they are stable enough to write about at this stage. 

- **Plenary session** at [12.30 UTC](https://www.timeanddate.com/worldclock/converter.html?iso=20211007T123000&p1=24&p2=438&p3=41&p4=1377&p5=241) [0730 CST, 1430 Europe, 1800 India, 2030 Taiwan]  
  - Discussion about framing of the meta-analysis paper (Introduction)
  - Continue interpreting results, potential Discussion points
  - Consolidate and finalize methods (and results?) sections

- **Small group meeting** for the N/S American time zones at 1030 AM CST  
  - Main goal will be till in gaps for the outline -- we will need to be flexible on this
  - Start outlining a roadmap for where to take this analysis/paper after the working group meeting

- **Overall deliverables** for Day 3 are a written outline for everything we have done so far, and a draft roadmap for the next steps. 

------------------------------

#### Thursday, 28 October

- **Small group meeting** for the Europe/Asia time zones at  [8:30 UTC](https://www.timeanddate.com/worldclock/converter.html?iso=20211007T083000&p1=24&p2=438&p3=41&p4=1377&p5=241) [1030 Europe, 1400 India, 1630 Taiwan]  
  - Discuss potential individual projects stemming off of the main meta-analysis
  - Consolidate efforts on the outline of work done so far
  - Add detail to roadmap 

- **Plenary session** at [12.30 UTC](https://www.timeanddate.com/worldclock/converter.html?iso=20211007T123000&p1=24&p2=438&p3=41&p4=1377&p5=241) [0730 CST, 1430 Europe, 1800 India, 2030 Taiwan]  
  - Discuss a timeline, identify tasks, and section leaders to drive the meta-analysis forward. 
  - Discussion about authorship for meta-analysis and other outputs.  
  - Discuss potential ways to tackle Objective 2 (Conceptual paper on using theory and experiments to study global change effects on PSF) 

- **Small group meeting** for the N/S American time zones at 1030 AM CST  
  - Discuss potential individual projects stemming off of the main meta-analysis 
  - Consolidate efforts on model and outline.


--------------------


---
layout: page
title: 
---

Link to time zone conversions: [click here](https://www.timeanddate.com/worldclock/converter.html?iso=20210916T140000&p1=5687&p2=24&p3=41&p4=1377&p5=313&p6=438&p7=505&p8=241) 

| Name                   | Location          | Contact                 |
| ----------------       | ----------------- | ----------------------  |
| György Barabás         | Linköping, Sweden | gyorgy.barabas@liu.se   |
| Adriana Corrales       | Bogotá, Colombia  | adricorrales33@gmail.com|
| Stan Harpole           | Leipzig, Germany  | stan.harpole@idiv.de    |
| Gaurav Kandlikar       | Columbia, MO, USA | gkandlikar@missouri.edu |
| Po-Ju Ke               | Taipei, Taiwan    | pojuke@ntu.edu.tw       |
| Meghna Krishnadas      | Hyderabad, India  | meghna@csirccmb.org      |
| Yadugiri V Tiruvaimozhi| Bengaluru, India  | vtyadu@gmail.com        |
| Xinyi Yan              | Austin, Texas, USA| xinyiyan@utexas.edu     |


---
layout: page
title: " "
---


## Welcome to the sPSF working group

*This site is under construction. Please let Gaurav know if you think of any resources that would be helpful to include here!* 

### Links 

- Zoom link: Remove spaces from the following: https://umsystemprotected.zoom.us /j/ 96955991913 ? pwd= MmV4am55NlRCNUFKdHIrOVVLRnRGdz09  
- Daily meeting notes: [link](https://docs.google.com/document/d/1ZB4RIKWlPH0WtJ1v0QegL8KUpHqvRd_DvO3CPzo_18w/edit?usp=sharing)  
- Readings for Meeting 1: [link](/reading-list/#core-papers)
- Link to github repository for Meta-analysis: TBD  
- Link to submitted proposal: [link](/assets/spsf-proposal-final.pdf).  
  *Check the group Slack or contact Gaurav for the password*. 

---
layout: page
title: 
---

Please contact Gaurav if you have any trouble accessing these papers.

#### Core papers 
*Please read these before the first working group meeting, if you are not already familiar with them*. 

- Bever et al. 1997. Incorporating the Soil Community into Plant Population Dynamics: The Utility of the Feedback Approach. [Link](https://doi.org/10.2307/2960528). 

- Crawford et al. 2019. When and where plant-soil feedback may promote plant coexistence: a meta-analysis. [Link](https://doi.org/10.1111/ele.13278)  

- Kandlikar et al. 2019. Winning and losing with microbes: How microbially mediated fitness differences influence plant diversity.  [Link](https://doi.org/10.1111/ele.13280)  
  
- Ke and Wan. 2020. Effects of soil microbes on plant competition: a perspective from modern coexistence theory. [Link]( https://doi.org/10.1002/ecm.1391)  

- Xi et al. 2021. Relationships between plant–soil feedbacks and functional traits. [Link](https://doi.org/10.1111/1365-2745.13731)  
  
#### Other relevant references   

- Barabas et al. 2018. Chesson's coexistence theory. [Link](https://doi.org/10.1002/ecm.1302)  

- Bever et al. 2010 Rooting theories of plant community ecology in microbial interactions. [Link](http://dx.doi.org/10.1016/j.tree.2010.05.004). 

- Chesson. 2018. Updates on mechanisms of maintenance of species diversity. [Link](https://doi.org/10.1111/1365-2745.13035)

- Meszéna et al. 2006.  Competitive exclusion and limiting similarity: a unified theory. [Link](https://pubmed.ncbi.nlm.nih.gov/16243372/)

- Lekberg et al. 2018. Relative importance of competition and plant–soil feedback, their synergy, context dependency and implications for coexistence. [Link](https://doi.org/10.1111/ele.13093)  

- Beals et al. 2020. Predicting Plant-Soil Feedback in the Field: Meta-Analysis Reveals That Competition and Environmental Stress Differentially Influence PSF. [Link](https://doi.org/10.3389/fevo.2020.00191)


#### References highlighted in group meeting conversations  
**Bolded references are ones that might be especially relevant** 

- **The fungal collaboration gradient dominates the root economics space in plants** [Link](https://www.science.org/doi/pdf/10.1126/sciadv.aba3756). *Useful framework for integrating root traits (especially root diameter, RD, and specific root length, SRL) into expectations for dependence on mutualists.*  
- Mycorrhizal traits and plant communities: perspectives for integration [Link](https://doi.org/10.1111/jvs.12177)
- Globally, plant‐soil feedbacks are weak predictors of plant abundance [Link](https://onlinelibrary.wiley.com/doi/pdf/10.1002/ece3.7167)
- A theoretical model linking interspecific variation in density dependence to species abundances [Link](https://link.springer.com/article/10.1007%2Fs12080-011-0119-z)
- **Microbiome influence on host community dynamics: Conceptual integration of microbiome feedback with classical host–microbe theory** [Link](https://doi.org/10.1111/ele.13891) *Raises some challenges to evaluating the PSF model in terms of niche and fitness differences.*
